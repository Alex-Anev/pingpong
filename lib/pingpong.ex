defmodule Pingpong do
  def start do
    p1_pid = spawn(fn -> p1(0) end)
    p2_pid = spawn(fn -> p2(0) end)
    # scoreboard_pid = spawn(fn -> score end)
    send(p1_pid, {:ping, p2_pid})
  end

  def p1(5) do 
    IO.puts "Player 1 wins"
  end

  def p1(score) do
    receive do
      {:ping, pid} ->
        IO.puts "ping #{score}|"
        :timer.sleep(Enum.random(100..700))
        send(pid, {:pong, self()})
    end
    score
    |> scoreboard()
    |> p1()
  end

  def p2(5) do
    # Process.exit(p1_pid, :kill)
    IO.puts "Player 2 wins"
  end

  def p2(score) do
    receive do
      {:pong, pid} ->
        IO.puts "      |#{score} pong"
        :timer.sleep(Enum.random(100..700))
        send(pid, {:ping, self()})
    end
    score
    |> scoreboard()
    |> p2()
  end

  def scoreboard(curr_score) do
    rng = Enum.random(1..5)
    case rng <= 2 do
      true -> curr_score + 1
      false -> curr_score
    end
  end

  # def score() do
  #   receive do
  #     {curr_score, pid} ->
  #       rng = Enum.random(1..5)
  #       if(rng == 3) do 
  #         send(pid, {:score, curr_score + 1})
  #       else
  #         send(pid, {:score, curr_score})
  #       end
  #   end
  # end
end
